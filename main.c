#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "library/library.hpp"
#include "library/readersStarvation.hpp"
#include "library/writersStarvation.hpp"
#include "library/noStarvation.hpp"

using namespace std;

void Usage(char *appName)
{
	printf(	"\nUsage: %s -w -r -t\n\n"
		"\t-w\tnumber of writers threads\n"
		"\t-r\tnumber of readers threads\n"
		"\t-t\tmode:\n"
		"\t\t\t[1] writers starvation\n"
		"\t\t\t[2] readers starvation\n"
		"\t\t\t[3] excluded starvation\n\n", appName);
	exit(EXIT_FAILURE);
}

void ExitReason(const char* message)
{
	printf("%s\n", message);
	exit(EXIT_FAILURE);
}

int main(int argc, char* argv[])
{
	int writersThreads = 20;
	int readersThreads = 10;
	int type = 1;

	int c;
	while((c = getopt(argc, argv, "w:r:t:?")) != -1)
	{
		switch(c)
		{
		case 'w':
			writersThreads = atoi(optarg);
			break;
		case 'r':
			readersThreads = atoi(optarg);
			break;
		case 't':
			type = atoi(optarg);
			if(type < 1 || type > 3)
				Usage(argv[0]);
			break;
		case '?':
		default:
			Usage(argv[0]);
			break;
		}
	}
	Library* lib;
	switch(type)
	{
	case 1:
		lib = new WritersStarvation(writersThreads, readersThreads);
		printf("Library mode: Writers starvation\n");
		break;
	case 2:
		lib = new ReadersStarvation(writersThreads, readersThreads);
		printf("Library mode: Readers starvation\n");
		break;
	case 3:
		lib = new NoStarvation(writersThreads, readersThreads);
		printf("Library mode: No starvation\n");
		break;
	}

	lib->Init();

	delete lib;

	return 0;
}
