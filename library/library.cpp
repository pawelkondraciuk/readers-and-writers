#include "library.hpp"
#include <stdio.h>
#include <errno.h>

Library::Library(int _WritersNo, int _ReadersNo)
		: ReadersNo(_ReadersNo), WritersNo(_WritersNo), WriterQ(0), ReaderQ(0), ReadersC(0), WritersC(0)
{
	pthread_mutex_init(&mutex, NULL);
	pthread_mutex_init(&incW, NULL);
	pthread_mutex_init(&incR, NULL);
	pthread_mutex_init(&q, NULL);
}
void* Library::ThreadRunner(void* arg)
{
	thread_args* args = (thread_args*)arg;

	if(args->type)
		((Library*)args->instance)->ReaderThread(args->id);
	else
		((Library*)args->instance)->WriterThread(args->id);

	delete args;
}
void Library::Init()
{
	pthread_t* readers = new pthread_t[ReadersNo];
	pthread_t* writers = new pthread_t[WritersNo];

	for(int i = 0; i < ReadersNo; i++)
		pthread_create(&readers[i], NULL, &Library::ThreadRunner,
				new thread_args(this, i, 1));

	for(int i = 0; i < WritersNo; i++)
		pthread_create(&writers[i], NULL, &Library::ThreadRunner,
				new thread_args(this, i, 0));

	for(int i = 0; i < ReadersNo; i++)
		pthread_join(readers[i], NULL);

	for(int i = 0; i < WritersNo; i++)
		pthread_join(writers[i], NULL);
}

void Library::ShowInfo()
{
	printf("ReaderQ: %d WriterQ: %d [in: R:%d W:%d]\n", ReaderQ, WriterQ, ReadersC, WritersC);
}
