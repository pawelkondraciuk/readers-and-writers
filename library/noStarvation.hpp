#include "library.hpp"
#include <queue>

using namespace std;

class NoStarvation : public Library
{
public:
	NoStarvation(int _writersNo, int _readersNo)
		: Library(_writersNo, _readersNo)
	{
		pthread_cond_init(&cond, NULL);
	 	pthread_mutex_init(&quse, NULL);
		libraryStatus = 0;
	 };

	void WriterThread(int id);
	void ReaderThread(int id);
protected:
	class Waiter;
private:
	int libraryStatus;
	pthread_cond_t cond;
	pthread_mutex_t quse;
	queue<pthread_t> toLibrary;
};

