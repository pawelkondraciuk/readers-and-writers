#include "readersStarvation.hpp"

void ReadersStarvation::WriterThread(int id)
{
	pthread_t t = pthread_self();

	while(1)
	{
		pthread_mutex_lock(&incW); 	// ochrona danych
		WriterQ++;			// wejscie do kolejki
		pthread_mutex_unlock(&incW);

		pthread_cond_broadcast(&cond); 	// sygnalizacja oczekującym wątkom wyjście z czytelni

		pthread_mutex_lock(&mutex);
		do
		{
			if(!WritersC && !ReadersC) // wyjście z kolejki możliwe gdy czytelnia jest pusta
				break;
			pthread_cond_wait(&cond, &mutex);
		}while(1);
		pthread_mutex_unlock(&mutex);

		pthread_mutex_lock(&incW); 	// ochrona danych
		WriterQ--; WritersC++;		// wejście do czytelni
		pthread_mutex_unlock(&incW);

		ShowInfo();

		pthread_mutex_lock(&incW); 	// ochrona danych
		WritersC--;			// wujście z czytelni
		pthread_mutex_unlock(&incW);
	}
}
void ReadersStarvation::ReaderThread(int id)
{
	pthread_t t = pthread_self();
	while(1)
	{
		pthread_mutex_lock(&incR);
		ReaderQ++;
		pthread_mutex_unlock(&incR);

		pthread_cond_broadcast(&cond);

		pthread_mutex_lock(&mutex);
		do
		{
			if(!WriterQ && !WritersC) 	// wyjście z kolejki możliwe gdy brak pisarzy w kolejce i czytelni
				break;
			pthread_cond_wait(&cond, &mutex);
		}while(1);
		pthread_mutex_unlock(&mutex);

		pthread_mutex_lock(&incW);
		ReaderQ--; ReadersC++;
		pthread_mutex_unlock(&incW);

		ShowInfo();

		pthread_mutex_lock(&incW);
		ReadersC--;
		pthread_mutex_unlock(&incW);
	}
}
