#include "library.hpp"

class ReadersStarvation : public Library
{
public:
	ReadersStarvation(int _writersNo, int _readersNo)
		: Library(_writersNo, _readersNo)
		{ pthread_cond_init(&cond, NULL); };

	void WriterThread(int id);
	void ReaderThread(int id);
private:
	pthread_cond_t cond;
};
