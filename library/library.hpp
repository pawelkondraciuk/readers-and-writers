#ifndef _LIBRARY_H_
#define _LIBRARY_H_

#include <pthread.h>

class Library
{
public:
	Library(int _WritersNo, int _ReadersNo);
	virtual void WriterThread(int id) = 0;
	virtual void ReaderThread(int id) = 0;
	static void* ThreadRunner(void* arg);

	void Init();
private:
	int ReadersNo;
	int WritersNo;
	class thread_args;
protected:
	int ReaderQ;
	int WriterQ;
	int ReadersC;
	int WritersC;
	pthread_mutex_t mutex;
	pthread_mutex_t incW;
	pthread_mutex_t incR;
	pthread_mutex_t q;
	void ShowInfo();
};


class Library::thread_args
{
public:
	Library* instance;
	int type;
	int id;

	thread_args(Library* _instance, int _id, int _type)
	: instance(_instance), id(_id), type(_type) { };
};

#endif // _LIBRARY_H_
