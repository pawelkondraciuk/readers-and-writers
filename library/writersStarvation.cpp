#include "writersStarvation.hpp"

void WritersStarvation::WriterThread(int id)
{
	pthread_t t = pthread_self();

	while(1)
	{
		pthread_mutex_lock(&incW);
		WriterQ++;
		pthread_mutex_unlock(&incW);

		pthread_cond_broadcast(&cond);

		pthread_mutex_lock(&mutex);
		do
		{
			if(!ReadersC && !ReaderQ && !WritersC)	// wyjście z kolejki możliwe gdy brak czytelników i pisarzy w kolejce oraz gdy czytelnia jest pusta
				break;
			pthread_cond_wait(&cond, &mutex);
		}while(1);
		pthread_mutex_unlock(&mutex);

		pthread_mutex_lock(&incW);
		WriterQ--; WritersC++;
		pthread_mutex_unlock(&incW);

		ShowInfo();

		pthread_mutex_lock(&incW);
		WritersC--;
		pthread_mutex_unlock(&incW);
	}
}
void WritersStarvation::ReaderThread(int id)
{
	pthread_t t = pthread_self();
	while(1)
	{
		pthread_mutex_lock(&incR);
		ReaderQ++;
		pthread_mutex_unlock(&incR);

		pthread_cond_broadcast(&cond);

		pthread_mutex_lock(&mutex);
		do
		{
			if(!WritersC)	// wyjście z kolejki możliwe gdy brak pisarzy w czytelni
				break;
			pthread_cond_wait(&cond, &mutex);
		}while(1);
		pthread_mutex_unlock(&mutex);

		pthread_mutex_lock(&incW);
		ReaderQ--; ReadersC++;
		pthread_mutex_unlock(&incW);

		ShowInfo();

		pthread_mutex_lock(&incW);
		ReadersC--;
		pthread_mutex_unlock(&incW);
	}
}
