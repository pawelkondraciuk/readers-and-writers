
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

void NoStarvation::WriterThread(int id)
{
	pthread_t t = pthread_self();

	while(1)
	{
	pthread_mutex_lock(&quse);	//sekcja krytyczna
	WriterQ++;			//zmiana wielkosci kolejki pisarzy
	toLibrary.push(t);		//watek do kolejki
	pthread_mutex_unlock(&quse);	//koniec sekcji krytycznej

	pthread_cond_broadcast(&cond);	//nadanie sygnalu

	pthread_mutex_lock(&mutex);	//zablokowanie mutexa do zmiennej warunkowej cond
	while(1)
	{
		if(pthread_equal(t, toLibrary.front()) && !WritersC && !ReadersC)
			break;		//gdy watek pierwszy w kolejce i pusto w czytelni
		pthread_cond_wait(&cond, &mutex);
	}
	pthread_mutex_unlock(&mutex);

	pthread_mutex_lock(&quse);	//sekcja krytyczna
	WriterQ--; WritersC++;		//wychodzenie z kolejki
	toLibrary.pop();
	pthread_mutex_unlock(&quse);	//koniec sekcji krytycznej

	ShowInfo();			//wyswietlanie danych
	pthread_mutex_lock(&quse);	//sekcja krytyczna (wyjscie)
	WritersC--;			//dekrementacja z biblioteki
	pthread_mutex_unlock(&quse);	//wyjscie z sekcji krytycznej
	//watek powraca
	}
}

void NoStarvation::ReaderThread(int id)
{
	pthread_t t = pthread_self();
	while(1)
	{
		pthread_mutex_lock(&quse);	//sekcja krytyczna
		ReaderQ++;			//inkrementacja wielkosci kolejkki
		toLibrary.push(t);		//do kolejki
		pthread_mutex_unlock(&quse);	//koniec sekcji krytycznej

		pthread_cond_broadcast(&cond); 	//wyslanie sygnalu obudzenia watkow

		pthread_mutex_lock(&mutex);	//start mutexa zmiennej warunkowej
		while(1)
		{
			if(!WritersC &&
				pthread_equal(t, toLibrary.front()))
			break;			//nie ma pisarzy w czytelni
			pthread_cond_wait(&cond, &mutex);
		}
		pthread_mutex_unlock(&mutex);

		pthread_mutex_lock(&quse);	//sekcja krytyczna
		toLibrary.pop();		//wyjecie z kolejki
		ReaderQ--;			//dekremenacja z kolejki
		ReadersC++;			//+1 do ilosci czytelnikow w czytelni
		pthread_mutex_unlock(&quse);	//koniec sekcji krytycznej

		ShowInfo();			//informacja o watku i czytelni

		sleep(1);

		pthread_mutex_lock(&quse);	//sekcja krytyczna
		ReadersC--;			//dekrementacja ilosci czytelnikow w czytelni
		pthread_mutex_unlock(&quse);	//koniec sekcji krytycznej
		//watek powraca
	}

}
